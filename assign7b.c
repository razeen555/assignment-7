#include <stdio.h>
int main() {
    char str[100], ch;
    int count = 0;

    printf("enter sentance: ");
    fgets(str, sizeof(str), stdin);

    printf("Enter a character = ");
    scanf("%c", &ch);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("frequency of %c = %d", ch, count);
    return 0;
}
